package linearalgebra;

/**
 *	Vector3d is a class that creates an object class Vector3d in the package linearalgebra
 *  @author   Simona Gragomir
 *  @version  9/09/2022
*/


public class Vector3d {
    private double x;
    private double y;
    private double z;
    
    // the constructor for Vector3d
    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // returns x
    public double getX() {  
        return this.x;  
    } 
    // returns y
    public double getY() {  
        return this.y;  
    } 
    // returns z
    public double getZ() {  
        return this.z;  
    } 

    // calculates the magnitude of the Vector3d
    public double magnitude() {
        return Math.sqrt(Math.pow(2,this.x) + Math.pow(2,this.y) + Math.pow(2,this.z));
    }

    // multiplies the new Vector3d to the old Vector3d and adds the results
    public double dotProduct(double x, double y, double z) {
        return (this.x * x) + (this.y * y) + (this.z * z); 
    }
    // adds the new Vector3d to the old Vector3d
    // returns a Vector3d object
    public Vector3d add(double x, double y, double z) {
        //creates the object
        Vector3d temp = new Vector3d(x + this.x, y + this.y, z + this.z);
        return temp;
    }
}
