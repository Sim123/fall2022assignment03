package linearalgebra;

/**
 *	test is a class that uses an object class called Vector3d
 *  @author   Simona Gragomir
 *  @version  9/09/2022
*/

public class test {
    public static void main(String[] args) {
        Vector3d r = new Vector3d(1,2,3);
        System.out.println(r.getX());
        System.out.println(r.getY());
        System.out.println(r.getZ());

        System.out.println(r.magnitude());
        System.out.println(r.dotProduct(4,5,6));

        System.out.println(r.add(4,5,6).getX());
        System.out.println(r.add(4,5,6).getY());
        System.out.println(r.add(4,5,6).getZ());
    }
}
