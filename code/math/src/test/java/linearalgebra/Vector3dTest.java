package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;


/**
 * Unit test for simple App.
 */

public class Vector3dTest {
    /**
     * Rigorous Test
     */

    final double TOLERANCE = .0000001;

    @Test
    public void testGetMethods() {
        Vector3d r = new Vector3d(1,2,3);
        assertEquals(1, r.getX(), TOLERANCE);
        assertEquals(2, r.getY(), TOLERANCE);
        assertEquals(3, r.getZ(), TOLERANCE);
    }

    @Test
    public void testMagnitude() {
        Vector3d r = new Vector3d(1,2,3);
        assertEquals(3.7416573867739413, r.magnitude(), TOLERANCE);
    }

    @Test
    public void testDotProduct() {
        Vector3d r = new Vector3d(1,2,3);
        assertEquals(32, r.dotProduct(4,5,6), TOLERANCE);
    }

    @Test
    public void testAdd() {
        Vector3d r = new Vector3d(1,2,3);
        assertEquals(5, r.add(4,5,6).getX(), TOLERANCE);
        assertEquals(7, r.add(4,5,6).getY(), TOLERANCE);
        assertEquals(9, r.add(4,5,6).getZ(), TOLERANCE);
    }

}